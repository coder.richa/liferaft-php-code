<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Http\Requests\StoreCustomerRequest;

// CustomerController to handle all the requests for customers API
class CustomerController extends APIController
{
     
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
        // Fetch all customers in the storage file.
        $customers = Customer::all();
        }catch(\Throwable $e){
           
            return $this->failureDataResponse("Error ocurred while reading customer's record.", 500);
        }
        // Return the customers in the response.
        return $this->successDataResponse($customers,200);
    }

       

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreCustomerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerRequest $request)
    {
    $validated = $request->validated();
    
    // Create a new customer.
    $isSaved = Customer::make($validated)->save();
    
    // Check if the customer is not saved. 
    if(!$isSaved):
        // Return the error response.
        return $this->failureDataResponse("Error ocurred while saving customer's record.", 500);
    endif;
    
    // Return the success response.
    return $this->successDataResponse(["message"=>"Record saved successfully!","customer"=>$validated],200);
    }


}