<?php

namespace App\Models;

use App\Traits\{StorageFileHandler,ArrayToObject};

// Customer class to store the customer details.
class Customer {

    // Contant to store the customer file name.
    private const CUSTOMER_DATA_FILENAME = 'customers.txt';

    // Attributes of the customer.
    public function __construct(public string $name ="", public string $email ="", public string $phone_number ="", public Address $address = new Address()){
        $this->email = $email;
        $this->name = $name;
        $this->phone_number = $phone_number;
        $this->address = $address;
    }

    // Method to retrieve the customer details in the storage file.
    public static function all() : array
    {   
        // Check if file exists 
        if (!StorageFileHandler::isExistingDataFile(self::CUSTOMER_DATA_FILENAME)) :
            return [];
        endif;
        
        // Read the data from the storage file.
        $fileData = StorageFileHandler::readJson(self::CUSTOMER_DATA_FILENAME);;
        
        // Convert the data to object.
        $customers = ArrayToObject::convert($fileData, Customer::class);

        // Return the customers.
        return $customers;
    }

    public static function make(array $data) : Customer
    {
        return new Customer($data['name'], $data['email'], $data['phone_number'], Address::make($data['address']));
    }

    // Method to store the customer details in the storage file.
    public function save() : bool
    {
        // Check if file exists 
        if (!StorageFileHandler::isExistingDataFile(self::CUSTOMER_DATA_FILENAME)) :
            // Create the file.
            StorageFileHandler::createFile(self::CUSTOMER_DATA_FILENAME);
        endif;

        // Read the data from the storage file.
        $customers = Customer::all();
        $customers[] = $this;
        return StorageFileHandler::writeJson(self::CUSTOMER_DATA_FILENAME,...$customers);
    }
}