<?php

namespace App\Models;

// Address class to store the address of the customer.
class Address{

    // Attributes of the address.
    public function __construct(public ?int $house_number = null, public ?string $street_name=null, public ?string $city=null, public ?string $state=null, public ?string $country=null){
        $this->house_number = $house_number;
        $this->street_name = $street_name;
        $this->city = $city;
        $this->state = $state;
        $this->country= $country;
    }

    public static function make(array $data) : Address
    {
        return new Address($data['house_number'], $data['street_name'], $data['city'], $data['state'], $data['country']);
    }
}