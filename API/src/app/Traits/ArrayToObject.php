<?php 
namespace App\Traits;

// ArrayToObject trait to convert array to object.
trait ArrayToObject{
    /**
     * This function will take an instance of a PHP stdClass and attempt to cast it to
     * the type of the specified $className.
     *
     * For example, we may pass 'Acme\Model\Product' as the $className.
     *
     * @param object $instance  an instance of the stdClass to convert
     * @param string $className the name of the class type to which we want to cals
     *
     * @return mixed a version of the incoming $instance casted as the specified className
     */
    private static function cast(object &$destination, \stdClass $source)
    {
        $sourceReflection = new \ReflectionObject($source);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty):
            $name = $sourceProperty->getName();
            // check if the property is an object
            if (gettype($destination->{$name}) == "object") :
            
                self::Cast($destination->{$name}, $source->$name);
            // if the property is not an object
            else:
                $destination->{$name} = $source->$name;
            endif;
        endforeach;
    }


    // Convert the array of stdClass objects to specific class.
    public static function convert(array $array, string $className) : array{
        foreach($array as $key => $value):
            $destination = new $className;
            self::cast($destination, $value);
            $array[$key] = $destination;
        endforeach;
       
        return $array;
    }
}