<?php 

namespace App\Traits;

// APIResponser trait to handle the API responses.
trait APIResponser{

    // Method to return the success response.
    private function successResponse($data, $code){
        return response()->json($data, $code);
    }

    // Method to return the error response.
    protected function errorResponse($message, $code){
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    // Method to format and send response.
    private function showAll(array $responseData, int $code, string $dataParameterName, bool $successStatus ){
        return $this->successResponse(['success'=>$successStatus,$dataParameterName=>$responseData], $code);
    }

    // Method to format and send success response.
    protected function successDataResponse($responseData, $code){
        return $this->showAll($responseData, $code,$dataParameterName="data",$successStatus=true);
    }

    // Method to format and send error response.
    protected function failureDataResponse($responseData, $code){
        return $this->showAll($responseData, $code,$dataParameterName="errors",$successStatus=true);
    }
}